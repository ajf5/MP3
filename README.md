# MP3 Distributed Maple Juice (Map Reduce)

## Executing Instructions

 * Setup Project
```
$ cd ~ && rm -r -f MP3 && git clone https://github.com/AFederici/MapReduce.git && cd MP3 && make all && chmod 777 Wc && ./Node
```

 * Running time commands
```
$ [join] join to a group via fixed introducer
$ [leave] leave the group
$ [id] print id (IP/PORT)
$ [member] print all membership list
$ [switch] switch to other mode (All-to-All to Gossip, and vice versa)
$ [mode] show in 0/1 [All-to-All/Gossip] modes
$ [exit] terminate process
$ === New since MP2 ===
$ [put] localfilename sdfsfilename
$ [get] sdfsfilename localfilename
$ [delete] sdfsfilename
$ [ls] list all machine (VM) addresses where this file is currently being stored
$ [lsall] list all sdfsfilenames with positions
$ [store] list all files currently being stored at this machine
$ === New since MP3 ===
$ [maple] start/queue a maple phase
$ [juice] start/queue a juice phase
$ [leader] print the leader/masters IP
```

 * MP3 Usage
 ```
 $ join //at each node
 $ put local sdfs //every file you want to process in maple, names as follows "dir_name"-"file_name"
 $ maple exe workers output_dir_name src_dir_name
 $ juice exe workers output_dir_name result_file_name delete_files={0,1} use_range={0,1}
 ```

 * Create files
```
$ dd if=/dev/urandom of=test_file bs=2097152 count=2
$ dd if=/dev/urandom of=test_file_07 bs=1000000 count=7
```


## Acknowledgement
 * [Beej's guide](http://beej.us/guide/bgnet/html/multi/index.html)
 * [Multiple Threads](https://www.tutorialspoint.com/cplusplus/cpp_multithreading.htm)
 * [String parser](https://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c)
 * [Wikicorpus](https://www.cs.upc.edu/~nlp/wikicorpus/)
