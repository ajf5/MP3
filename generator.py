import random

sample = ['a', 'b', 'c']
with open("./votes.txt", "w") as f:
    for i in range(0, 10):
        cur_vote = random.sample(sample, 3)
        data = ""
        for v in cur_vote:
            if data == "":
                data += v
            else:
                data += "=="
                data += v
        data += "\n"
        f.write(data)

