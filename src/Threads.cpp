#include "../inc/Node.h"

void *runUdpServer(void *udpSocket)
{
	UdpSocket* udp;
	udp = (UdpSocket*) udpSocket;
	udp->bindServer(PORT);
	pthread_exit(NULL);
}

void *runTcpServer(void *tcpSocket)
{
	TcpSocket* tcp;
	tcp = (TcpSocket*) tcpSocket;
	tcp->bindServer(TCPPORT);
	pthread_exit(NULL);
}

void *runTcpSender(void *tcpSocket)
{
	TcpSocket* tcp;
	tcp = (TcpSocket*) tcpSocket;
	while (1) {
		while (!tcp->mapleMessages.empty()) {
			vector<string> msgSplit = splitString(tcp->mapleMessages.front(), "::");
			if (msgSplit[0].compare("JUICE") == 0){
				string removeSender = tcp->mapleMessages.front().substr(msgSplit[0].size() + 2 + msgSplit[1].size() + 2);
				Messages msg(JUICE, removeSender);
				tcp->sendMessage(msgSplit[1], TCPPORT, msg.toString());
			} else{
				string removeSender = tcp->mapleMessages.front().substr(msgSplit[0].size() + 2);
				Messages msg(CHUNK, removeSender);
				//processor, exec, file, start, prefix
				tcp->sendMessage(msgSplit[0], TCPPORT, msg.toString());
			}
			tcp->mapleMessages.pop();
		}
		while (!tcp->pendSendMessages.empty()) {
			//tcp->putFile(msgSplit[0], TCPPORT, msgSplit[1], msgSplit[2], msgSplit[3]);
			//IP, local, sdfs, remote
			vector<string> msgSplit = splitString(tcp->pendSendMessages.front(), "::");
			FILE *fp = fopen(msgSplit[1].c_str(), "rb");
			fseek(fp, 0, SEEK_END);
			int size = ftell(fp);
			fseek(fp, 0, SEEK_SET);
			fclose(fp);
			time_t fileTimestamp;
			time(&fileTimestamp);
			string localfilename = msgSplit[1], sdfsfilename = msgSplit[2], overwritefilename = msgSplit[3];
			localfilename = sdfsfilename+"_"+to_string(fileTimestamp);
			if (overwritefilename.compare("") != 0) localfilename = overwritefilename;
			string header = sdfsfilename + "," + msgSplit[1];
			string toSend = msgSplit[1] + "," + to_string(size);
			//cout << "[DEBUG] " << toSend << endl;
			string forwardMsg = msgSplit[0] + "::" + to_string(PUTACK) + "::" + localfilename + "::" + header + "::" + toSend + "::";
			tcp->pendSendMessages.pop();
			tcp->mergeMessages.push(forwardMsg);
		}
		while (!tcp->mergeMessages.empty()) {
			vector<string> msgSplit = splitString(tcp->mergeMessages.front(), "::");
			tcp->mergeFiles(msgSplit[0], TCPPORT, msgSplit[1], msgSplit[2], msgSplit[3], msgSplit[4], msgSplit[5]);
			tcp->mergeMessages.pop();
		}
	}
	pthread_exit(NULL);
}

void *runSenderThread(void *node)
{
	// acquire node object
	Node *nodeOwn = (Node *) node;
	nodeOwn->activeRunning = true;

	// heartbeat to introducer to join the system
	Member introducer(getIP(INTRODUCER), PORT);
	nodeOwn->joinSystem(introducer);

	while (nodeOwn->activeRunning) {

		// 1. deepcopy and handle queue, and
		// 2. merge membership list
		nodeOwn->listenToHeartbeats();

		// Volunteerily leave
		if(nodeOwn->activeRunning == false){
			pthread_exit(NULL);
		}

		//add failure detection in between listening and sending out heartbeats
		nodeOwn->failureDetection();

		// keep heartbeating
		nodeOwn->localTimestamp++;
		nodeOwn->heartbeatCounter++;
		nodeOwn->updateNodeHeartbeatAndTime();

		// 3. prepare to send heartbeating, and
		// 4. do gossiping
		nodeOwn->heartbeatToNode();

		//5a. check for queue maple/juice messages
		nodeOwn->handleMaplejuiceQ();
		//5b. check for regular TCP messages
		nodeOwn->handleTcpMessage();

		// 6. check leader (If hashRing is sent via heartbeat, then we have a leader)
		if (!nodeOwn->checkLeaderExist()) { // If no leader
			nodeOwn->tcpElectionProcessor();
			if (nodeOwn->findWillBeLeader()) {
				if (nodeOwn->localTimestamp - nodeOwn->electedTime > T_election)  { // when entering to stable state
					if (nodeOwn->localTimestamp - nodeOwn->proposedTime > T_election) {
						nodeOwn->proposeToBeLeader();
						nodeOwn->proposedTime = nodeOwn->localTimestamp;
					}
				}
			}
		}

		// 7. bandwidth and mode switch handled (optional)
		time_t endTimestamp;
		time(&endTimestamp);
		double diff = difftime(endTimestamp, nodeOwn->startTimestamp);
		computeAndPrintBW(nodeOwn, diff);
		if (nodeOwn->prepareToSwitch) {
			cout << "[SWITCH] I am going to swtich my mode in " << T_switch << "s" << endl;
			nodeOwn->SwitchMyMode();
		} else {
			usleep(T_period);
		}
	}
	pthread_exit(NULL);
}
